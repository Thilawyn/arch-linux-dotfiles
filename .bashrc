#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ssh="TERM=xterm-256color ssh"
alias dcd="docker-compose -f docker-compose.dev.yml"
alias dcp="docker-compose -f docker-compose.prod.yml"
alias drop-caches="sudo sh -c 'sync; echo 3 > /proc/sys/vm/drop_caches'"

# alias ls='ls --color=auto'
alias ls="lsd"
PS1="[\u@\h \W]\$ "

# Node
source /usr/share/nvm/init-nvm.sh

# Wal color scheme
cat ~/.cache/wal/sequences
