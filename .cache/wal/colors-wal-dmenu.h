static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#b1c3be", "#000015" },
	[SchemeSel] = { "#b1c3be", "#B81655" },
	[SchemeOut] = { "#b1c3be", "#3C4CE3" },
};
