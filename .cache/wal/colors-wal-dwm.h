static const char norm_fg[] = "#b1c3be";
static const char norm_bg[] = "#000015";
static const char norm_border[] = "#7b8885";

static const char sel_fg[] = "#b1c3be";
static const char sel_bg[] = "#23238D";
static const char sel_border[] = "#b1c3be";

static const char urg_fg[] = "#b1c3be";
static const char urg_bg[] = "#B81655";
static const char urg_border[] = "#B81655";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
