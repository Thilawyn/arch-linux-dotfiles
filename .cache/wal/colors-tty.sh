#!/bin/sh
[ "${TERM:-none}" = "linux" ] && \
    printf '%b' '\e]P0000015
                 \e]P1B81655
                 \e]P223238D
                 \e]P3443C8B
                 \e]P4544DA2
                 \e]P51B5BF8
                 \e]P63C4CE3
                 \e]P7b1c3be
                 \e]P87b8885
                 \e]P9B81655
                 \e]PA23238D
                 \e]PB443C8B
                 \e]PC544DA2
                 \e]PD1B5BF8
                 \e]PE3C4CE3
                 \e]PFb1c3be
                 \ec'
