const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#000015", /* black   */
  [1] = "#B81655", /* red     */
  [2] = "#23238D", /* green   */
  [3] = "#443C8B", /* yellow  */
  [4] = "#544DA2", /* blue    */
  [5] = "#1B5BF8", /* magenta */
  [6] = "#3C4CE3", /* cyan    */
  [7] = "#b1c3be", /* white   */

  /* 8 bright colors */
  [8]  = "#7b8885",  /* black   */
  [9]  = "#B81655",  /* red     */
  [10] = "#23238D", /* green   */
  [11] = "#443C8B", /* yellow  */
  [12] = "#544DA2", /* blue    */
  [13] = "#1B5BF8", /* magenta */
  [14] = "#3C4CE3", /* cyan    */
  [15] = "#b1c3be", /* white   */

  /* special colors */
  [256] = "#000015", /* background */
  [257] = "#b1c3be", /* foreground */
  [258] = "#b1c3be",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
