#!/usr/bin/env sh

# Path
export PATH=$(du ~/.local/bin | cut -f2 | tr '\n' ':' | sed 's/:*$//'):\
~/.cargo/bin:\
~/Android/Sdk/platform-tools:\
~/Android/Sdk/emulator:\
$PATH

systemctl --user import-environment PATH

# Other environment variables
export MPD_HOST=~/.mpd/socket

# Start the GUI if on TTY1
if [ -z $WAYLAND_DISPLAY ]; then
    [ "$(tty)" = "/dev/tty1" ] && exec startplasma-wayland
#   [ "$(tty)" = "/dev/tty2" ] && QT_QPA_PLATFORM=xcb exec exec-gamescope-session moonlight
#   [ "$(tty)" = "/dev/tty3" ] && exec exec-gamescope-session steam-embedded
fi
